var Cylon = require('cylon');
 
Cylon.robot({
  connections: {
    bluetooth: { adaptor: 'central', uuid: 'yourbb8uuid', module: 'cylon-ble' }
  },
 
  devices: {
    bb8: { driver: 'bb8', module: 'cylon-sphero-ble' }
  },
 
  work: function(my) {
    my.bb8.color("red");
    my.bb8.startCalibration();
  }
}).start();
