var Cylon = require('cylon');
var Led = require('./ledArduinoBB8');
var Custom = require('./customRandomColors');
 
Cylon.robot({
  connections: {
    bluetooth: { adaptor: 'central', uuid: 'yourbb8uuid', module: 'cylon-ble' },
    arduino: { adaptor: 'firmata', port: '/dev/ttyACM0' }
  },
 
  devices: {
    bb8: { driver: 'bb8', module: 'cylon-sphero-ble', connection: 'bluetooth'},
    whiteLed: { driver: 'led', pin: 13, connection: 'arduino'},
    blueLed: { driver: 'led', pin: 12, connection: 'arduino'},
    redLed: { driver: 'led', pin: 11, connection: 'arduino'},
    yellowLed: { driver: 'led', pin:10, connection: 'arduino'},
    greenLed: { driver: 'led', pin: 9, connection: 'arduino'} 
  },

  work: function(my) {
    var nuevoColor = "";
  		every(1000, function(){
        Custom.randomColor(my.bb8);
  			my.bb8.getColor(function(err, data){
  				console.log(data.color);
          nuevoColor = data.color;
          Led.encender(nuevoColor, my.whiteLed, my.blueLed, my.redLed, my.yellowLed, my.greenLed);
          
  			});
        
  			
  		}
  	)
 }
}).start();
