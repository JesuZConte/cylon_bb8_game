var Cylon = require('cylon');
var Move = require('./move');
 
Cylon.robot({
  connections: {
    bluetooth: { adaptor: 'central', uuid: 'yourbb8uuid', module: 'cylon-ble' },
    keyboard: { adaptor: 'keyboard' }
  },
 
  devices: {
    bb8: { driver: 'bb8', module: 'cylon-sphero-ble', connection:'bluetooth' },
    keyboard: { driver: 'keyboard', connection: 'keyboard' }
   },

  work: function(my) {
  	my.bb8.color('red');
  	my.bb8.startCalibration();
    Move.left(my.keyboard, my.bb8);

    Move.right(my.keyboard, my.bb8);

    Move.up(my.keyboard, my.bb8);

    Move.down(my.keyboard, my.bb8);

    Move.stop(my.keyboard, my.bb8);

    Move.spinLeft(my.keyboard, my.bb8);

    Move.spinRight(my.keyboard, my.bb8);

    Move.randomColor(my.keyboard, my.bb8);
  }
}).start(); 
