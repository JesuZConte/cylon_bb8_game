module.exports = {
	random: function(device){
		device.roll(60, Math.floor(Math.random() * 360));
	},

	left: function(keyboard, device){
		keyboard.on('left', function(key) {
      	device.roll(100,270);
    	});
	},

	right: function(keyboard, device){
		keyboard.on('right', function(key) {
      	device.roll(100,90);
    	});
	},

	up: function(keyboard, device){
		keyboard.on('up', function(key) {
      	device.roll(100,0);
    	});
	},

	down: function(keyboard, device){
		keyboard.on('down', function(key) {
      	device.roll(100,180);
    	});
	},

	stop: function(keyboard, device){
		keyboard.on('space', function(key) {
      	device.stop();
    	});
	},

	spinLeft: function(keyboard, device){
		keyboard.on('a', function(key){
    	device.spin("left", 90);
   	 	});
	},

	spinRight: function(keyboard, device){
		keyboard.on('s', function(key){
    	device.spin("right", 90);
    	});
	},

	randomColor: function(keyboard, device){
		keyboard.on('d', function(key){
    	device.randomColor();
    	});
	},
}