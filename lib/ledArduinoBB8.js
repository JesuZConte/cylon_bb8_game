module.exports = {
	encender : function(color, white, blue, red, yellow, green){

				if(color == 0xf0f8ff   || color == 0x7fffd4 || color == 0x0000ff || color == 0x8a2be2 ||
					color == 0x5f9ea0 || color == 0x6495ed || color == 0x00008b || color == 0x8b008b || color == 0x9932cc || color == 0x483d8b ||
					color == 0x9400d3 || color == 0x00bfff || color == 0x1e90ff || color == 0x4b0082 || color == 0xadd8e6 || color == 0x87cefa ||
					color == 0xb0c4de || color == 0x0000cd || color == 0xba55d3 || color == 0x9370db || color == 0x7b68ee || color == 0x191970 ||
					color == 0x000080 || color == 0xb0e0e6 || color == 0x800080 || color == 0x663399 || color == 0x4169e1 || color == 0x87ceeb ||
					color == 0x6a5acd || color == 0x4682b4){
	  				console.log("azul");
	  				blue.turnOn();
	  				red.turnOff();
	  				green.turnOff();
	  				yellow.turnOff();
	  				white.turnOff();
	  			}
	  			if(color == 0xfaebd7 || color == 0xffe4c4 || color == 0xffebcd || color == 0xff7f50 || color == 0xdc143c || color == 0x8b0000 ||
	  				color == 0xe9967a || color == 0xff1493 || color == 0xb22222 || color == 0xff00ff || color == 0xff69b4 || color == 0xcd5c5c ||
	  				color == 0xf08080 || color == 0xffb6c1 || color == 0xffa07a || color == 0xff00ff || color == 0x800000 || color == 0xc71585 ||
	  				color == 0xff4500 || color == 0xda70d6 || color == 0xdb7093 || color == 0xffc0cb || color == 0xff0000 || color == 0xbc8f8f ||
	  				color == 0xfa8072 || color == 0xff6347 || color == 0xee82ee){
	  				console.log("rojo");
	  				red.turnOn();
	  				blue.turnOff();
	  				green.turnOff();
	  				yellow.turnOff();
	  				white.turnOff();
	  			}
	  			if(color == 0x7fff00 || color == 0x00ffff || color == 0x00ffff || color == 0xf0ffff || color == 0x008b8b || color == 0x006400 ||
	  				color == 0x556b2f || color == 0x8fbc8f || color == 0x00ced1 || color == 0x228b22 || color == 0x008000 || color == 0xadff2f ||
	  				color == 0x7cfc00 || color == 0xe0ffff || color == 0x90ee90 || color == 0x20b2aa || color == 0x00ff00 || color == 0x32cd32 ||
	  				color == 0x66cdaa || color == 0x3cb371 || color == 0x00fa9a || color == 0x48d1cc || color == 0x808000 || color == 0x6b8e23 ||
	  				color == 0x98fb98 || color == 0xafeeee || color == 0x2e8b57 || color == 0x00ff7f || color == 0x008080 || color == 0x40e0d0 ||
	  				color == 0x9acd32){
	  				console.log("verde");
	  				green.turnOn();
	  				red.turnOff();
	  				blue.turnOff();
	  				yellow.turnOff();
	  				white.turnOff();
	  			}
	  			if(color == 0xf5f5dc || color == 0xb8860b || color == 0xbdb76b || color == 0xff8c00 || color == 0xffd700 || color == 0xdaa520 ||
	  				color == 0xf0e68c || color == 0xfffacd || color == 0xfafad2 || color == 0xffffe0 || color == 0xffa500 || color == 0xeee8aa ||
	  				color == 0xcd853f || color == 0x8b4513 || color == 0xf4a460 || color == 0xa0522d || color == 0xd2b48c || color == 0xffff00){
	  				console.log("amarillo");
	  				yellow.turnOn();
	  				red.turnOff();
	  				green.turnOff();
	  				blue.turnOff();
	  				white.turnOff();
	  			}
	  			if(color == 0xfff8dc || color == 0xa9a9a9 || color == 0x2f4f4f || color == 0xfffaf0 || color == 0xdcdcdc || color == 0xf8f8ff ||
	  			 color == 0xf0fff0 || color == 0xfffff0 || color == 0xe6e6fa || color == 0xfff0f5 || color == 0xd3d3d3 || color == 0xfaf0e6 ||
	  			 color == 0xf5fffa || color == 0xffe4e1 || color == 0xffe4b5 || color == 0xffdead || color == 0xfdf5e6 || color == 0xffefd5 ||
	  			 color == 0xffdab9 || color == 0xdda0dd || color == 0xfff5ee || color == 0xc0c0c0 || color == 0x708090 || color == 0xfffafa ||
	  			 color == 0xd8bfd8 || color == 0xf5deb3 || color == 0xffffff || color == 0xf5f5f5){
	  				console.log("blanco");
	  				white.turnOn();
	  				red.turnOff();
	  				green.turnOff();
	  				yellow.turnOff();
	  				blue.turnOff();
	  			}

	  			if(color == 0xffe4c4 || color == 0xa52a2a || color == 0xdeb887 || color == 0xd2691e || color == 0x696969 || color == 0x808080 ||
	  				color == 0x778899){
	  				console.log("negro");
	  				white.turnOff();
	  				red.turnOff();
	  				green.turnOff();
	  				yellow.turnOff();
	  				blue.turnOff();
	  			}
			}

	}
