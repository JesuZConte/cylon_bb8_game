# README #

This project contains some files to program Sphero BB-8, Keyboard and Arduino Mega using Cylon.js framework.

Install the following:

**nodejs** https://nodejs.org/es/download/package-manager/

**gort** http://gort.io/

**cylonjs** https://cylonjs.com/

**firmata** https://github.com/hybridgroup/cylon-firmata

**cylonble** https://github.com/hybridgroup/cylon-ble

**cylon bb8** https://cylonjs.com/documentation/drivers/bb8/

**cylon keyboard** https://github.com/hybridgroup/cylon-keyboard



If you are using Ubuntu you can have problems with the USB detection. Follow the instructions in the following link to set it up. https://bugs.launchpad.net/ubuntu/+source/arduino/+bug/1054988